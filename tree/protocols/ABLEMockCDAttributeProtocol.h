/* Generated by RuntimeBrowser.
 */

@protocol ABLEMockCDAttributeProtocol

@required

- (NSArray *)repeatedStatistic:(int)arg1 forHistoryWindow:(CDDHistoryWindow *)arg2 error:(id*)arg3;
- (NSDictionary *)statistic:(int)arg1 forHistoryWindow:(CDDHistoryWindow *)arg2 error:(id*)arg3;

@end
