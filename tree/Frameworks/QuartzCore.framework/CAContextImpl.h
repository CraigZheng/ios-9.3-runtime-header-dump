/* Generated by RuntimeBrowser
   Image: /System/Library/Frameworks/QuartzCore.framework/QuartzCore
 */

@interface CAContextImpl : CAContext {
    struct Context { unsigned int x1; struct Context {} *x2; struct Mutex { struct _opaque_pthread_mutex_t { long x_1_2_1; BOOL x_1_2_2[40]; } x_3_1_1; } x3; struct Weak<const void *> { id x_4_1_1; unsigned int x_4_1_2; unsigned int x_4_1_3; unsigned int x_4_1_4; id x_4_1_5; struct Context {} x_4_1_6; struct CGColorSpace {} *x_4_1_7; struct __CFDictionary {} *x_4_1_8; unsigned int x_4_1_9; unsigned int x_4_1_10; unsigned int x_4_1_11; struct ObjectCache {} *x_4_1_12; id x_4_1_13; float x_4_1_14; struct Commit {} *x_4_1_15; struct Generic { int (**x_16_2_1)(); struct Context {} *x_16_2_2; } x_4_1_16; unsigned int x_4_1_17 : 1; unsigned int x_4_1_18 : 1; unsigned int x_4_1_19 : 1; unsigned int x_4_1_20 : 1; unsigned int x_4_1_21 : 1; } x4; /* Warning: Unrecognized filer type: '' using 'void*' */ void*x5; void*x6; void*x7; void*x8; void*x9; void*x10; void*x11; void*x12; void*x13; void*x14; void*x15; void*x16; void*x17; void*x18; void*x19; void*x20; } * _impl;
}

- (BOOL)colorMatchUntaggedContent;
- (struct CGColorSpace { }*)colorSpace;
- (unsigned int)contextId;
- (unsigned int)createFencePort;
- (unsigned int)createImageSlot:(struct CGSize { float x1; float x2; })arg1 hasAlpha:(BOOL)arg2;
- (unsigned int)createSlot;
- (void)dealloc;
- (void)deleteSlot:(unsigned int)arg1;
- (id)initRemoteWithOptions:(id)arg1;
- (id)initWithOptions:(id)arg1 localContext:(bool)arg2;
- (void)invalidate;
- (void)invalidateFences;
- (BOOL)isSecure;
- (id)layer;
- (float)level;
- (id)options;
- (void)orderAbove:(unsigned int)arg1;
- (void)orderBelow:(unsigned int)arg1;
- (struct Context { }*)renderContext;
- (void)setColorMatchUntaggedContent:(BOOL)arg1;
- (void)setColorSpace:(struct CGColorSpace { }*)arg1;
- (void)setFence:(unsigned int)arg1 count:(unsigned int)arg2;
- (void)setFencePort:(unsigned int)arg1;
- (void)setFencePort:(unsigned int)arg1 commitHandler:(id /* block */)arg2;
- (void)setLayer:(id)arg1;
- (void)setLevel:(float)arg1;
- (void)setObject:(id)arg1 forSlot:(unsigned int)arg2;
- (void)setSecure:(BOOL)arg1;
- (BOOL)valid;

@end
