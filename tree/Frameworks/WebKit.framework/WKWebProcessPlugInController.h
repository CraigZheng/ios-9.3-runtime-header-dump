/* Generated by RuntimeBrowser
   Image: /System/Library/Frameworks/WebKit.framework/WebKit
 */

@interface WKWebProcessPlugInController : NSObject <WKObject> {
    struct ObjectStorage<WebKit::InjectedBundle> { 
        struct type { 
            unsigned char __lx[52]; 
        } data; 
    }  _bundle;
    struct RetainPtr<id<WKWebProcessPlugIn> > { 
        void *m_ptr; 
    }  _principalClassInstance;
}

@property (readonly) struct Object { int (**x1)(); id x2; /* Warning: Unrecognized filer type: '' using 'void*' */ void*x3; void*x4; void*x5; void*x6; void*x7; void*x8; void*x9; void*x10; void*x11; void*x12; void*x13; void*x14; void*x15; void*x16; void*x17; void*x18; void*x19; unsigned char x20; void*x21; unsigned short x22; void*x23; short x24; void*x25; void*x26; void*x27; void*x28; unsigned long x29; int x30; unsigned int x31/* : ? */; const void*x32; const void*x33; void*x34; void*x35; const int x36; void x37; void*x38; void*x39; void*x40; void*x41; const void*x42; void*x43; void*x44; void*x45; out const void*x46; short x47; void*x48; void*x49; void*x50; short x51; void*x52; out in short x53; void*x54; void*x55; int x56; void*x57; void*x58; float x59; const void*x60; void*x61; void*x62; void*x63; out const void*x64; void*x65; void*x66; void*x67; short x68; void*x69; out in short x70; void*x71; void*x72; int x73; void*x74; void*x75; void*x76; void*x77; void*x78; void*x79; void*x80; }*_apiObject;
@property (readonly) struct OpaqueWKBundle { }*_bundleRef;
@property (readonly) WKConnection *connection;
@property (readonly, copy) NSString *debugDescription;
@property (readonly, copy) NSString *description;
@property (readonly) unsigned int hash;
@property (readonly) id parameters;
@property (readonly) Class superclass;

- (id).cxx_construct;
- (void).cxx_destruct;
- (struct Object { int (**x1)(); id x2; /* Warning: Unrecognized filer type: '' using 'void*' */ void*x3; void*x4; void*x5; void*x6; void*x7; void*x8; void*x9; void*x10; void*x11; void*x12; void*x13; void*x14; void*x15; void*x16; void*x17; void*x18; void*x19; void*x20; void*x21; void*x22; void*x23; void*x24; void*x25; void*x26; void*x27; void*x28; void*x29; void*x30; void*x31; void*x32; void*x33; void*x34; void*x35; void*x36; void*x37; void*x38; void*x39; void*x40; void*x41; void*x42; void*x43; unsigned char x44; void*x45; void*x46; void*x47; void*x48; void*x49; }*)_apiObject;
- (struct OpaqueWKBundle { }*)_bundleRef;
- (void)_setPrincipalClassInstance:(id)arg1;
- (id)connection;
- (void)dealloc;
- (id)parameters;

@end
